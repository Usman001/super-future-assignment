import PostActionTypes from "redux/postSlice/post.types";

const INITIAL_STATE = {
  posts: null,
  deletedPost: null,
  createPost: null,
  updatePost: null,
  postDetails: null,
  isPostDetailsLoading: false,
  isPostCreating: false,
  isPostUpdating: false,
  isPostsLoading: false,
  isPostDeleting: false,
};

/*************************************************/

const userReducer = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case PostActionTypes.GET_POSTS_SUCCESS:
      return {
        ...state,
        posts: action.payload,
      };
    case PostActionTypes.GET_POSTS_LOADING_START:
      return {
        ...state,
        isPostsLoading: true,
      };
    case PostActionTypes.GET_POSTS_LOADING_STOP:
      return {
        ...state,
        isPostsLoading: false,
      };
    case PostActionTypes.POST_DELETE_SUCCESS:
      return {
        ...state,
        deletedPost: action.payload,
      };
    case PostActionTypes.POST_DELETE_LOADING_START:
      return {
        ...state,
        isPostDeleting: true,
      };
    case PostActionTypes.POST_DELETE_LOADING_STOP:
      return {
        ...state,
        isPostDeleting: false,
      };
    case PostActionTypes.POST_DETAILS_SUCCESS:
      return {
        ...state,
        postDetails: action.payload,
      };
    case PostActionTypes.POST_DETAILS_LOADING_START:
      return {
        ...state,
        isPostDetailsLoading: true,
      };
    case PostActionTypes.POST_DETAILS_LOADING_STOP:
      return {
        ...state,
        isPostDetailsLoading: false,
      };
    case PostActionTypes.POST_CREATE_SUCCESS:
      return {
        ...state,
        createPost: action.payload,
      };
    case PostActionTypes.POST_CREATE_LOADING_START:
      return {
        ...state,
        isPostCreating: true,
      };
    case PostActionTypes.POST_CREATE_LOADING_STOP:
      return {
        ...state,
        isPostCreating: false,
      };
    case PostActionTypes.POST_UPDATE_SUCCESS:
      return {
        ...state,
        updatePost: action.payload,
      };
    case PostActionTypes.POST_UPDATE_LOADING_START:
      return {
        ...state,
        isPostUpdating: true,
      };
    case PostActionTypes.POST_UPDATE_LOADING_STOP:
      return {
        ...state,
        isPostUpdating: false,
      };
    default:
      return state;
  }
};

/*************************************************/

export default userReducer;

/*************************************************/
