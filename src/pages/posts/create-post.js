import React, { useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

// Components
import { Input, Card, Alert } from "antd";
// Redux
import * as PostsActions from "redux/postSlice/post.actions";
const { TextArea } = Input;

const CreatePost = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isCreated, setIsCreated] = useState(false);

  const isPostCreating = useSelector(
    (state) => state.postsSlice.isPostCreating
  );
  const createPost = useSelector((state) => state.postsSlice.createPost);

  const initialValues = {
    title: "",
    body: "",
    userId: "",
  };

  const validationSchema = Yup.object().shape({
    title: Yup.string().required("Required"),
    body: Yup.string().required("Required"),
    userId: Yup.string().required("Required"),
  });

  const onFormSubmit = (values, { resetForm }) => {
    dispatch(
      PostsActions.createPostRequest({
        formData: {
          title: values.title,
          body: values.body,
          userId: values.userId,
        },
        callback: () => {
          resetForm();
          setIsCreated(true);

          const timer = setTimeout(() => {
            setIsCreated(false);
            clearTimeout(timer);
          }, 5000);
        },
      })
    );
  };

  return (
    <>
      <div className="container mt-5">
        <Card title="Create Post" bordered={false}>
          {isCreated && (
            <>
              <Alert message="Post has created" type="success" />
              <br />
            </>
          )}
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onFormSubmit}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              values,
              touched,
              errors,
              isValid,
              dirty,
            }) => (
              <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                <Input
                  type="number"
                  placeholder="User ID"
                  name="userId"
                  onChange={handleChange}
                  value={values.userId}
                  status={errors.userId ? "error" : ""}
                />
                <p className="validation-text">{errors.userId}</p>
                <Input
                  type="text"
                  placeholder="Title"
                  name="title"
                  onChange={handleChange}
                  value={values.title}
                  status={errors.title ? "error" : ""}
                />
                <p className="validation-text">{errors.title}</p>
                <TextArea
                  rows={4}
                  type="text"
                  placeholder="Body"
                  name="body"
                  onChange={handleChange}
                  value={values.body}
                  status={errors.body ? "error" : ""}
                />
                <p className="validation-text">{errors.body}</p>
                <button
                  className="btn-primary"
                  type="submit"
                  disabled={!isValid || isPostCreating}
                >
                  {isPostCreating ? "Creating..." : "Submit"}
                </button>
              </form>
            )}
          </Formik>
        </Card>
      </div>
    </>
  );
};

export default CreatePost;
